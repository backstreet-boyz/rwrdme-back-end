
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('habits').del()
    .then(function () {
      // Inserts seed entries
      return knex('habits').insert([
        { habitName: 'Lifting weights', cue: 'Morning', duration: '5'},
        { habitName: 'Reading', cue: 'Morning', duration: '5'},
        { habitName: 'Meditation', cue: 'Morning', duration: '5'}
      ]);
    });
};
