
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('habits').del()
    .then(function () {
      // Inserts seed entries
      return knex('habits').insert([
        { habitName: 'Reading'},
        { habitName: 'Meditation'},
        { habitName: 'Sitting'}
      ]);
    });
};
