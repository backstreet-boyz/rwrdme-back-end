
const addBaseColumns = (knex, table) => {
  table.timestamp('createdAt').index().notNullable().defaultTo(knex.fn.now());
  table.timestamp('updatedAt').index().notNullable().defaultTo(knex.fn.now());
};

exports.up = function(knex) {
  console.log('Creating intial migration');

  return knex.schema
  .createTableIfNotExists('users', function (table) {
    table.bigIncrements('id').primary();
    table.string('email').unique();
    table.string('password');
    table.string('googleId');
    table.string('facebookId');
    addBaseColumns(knex, table);
  }).createTableIfNotExists('habits', function (table) {
    table.bigIncrements('id').primary();
    table.string('habitName');
    table.integer('timesDoneSuccessively');
    table.string('cue');
    table.integer('duration');
    table.string('reward');
    table.timestamp('lastCompleted');
    addBaseColumns(knex, table);
  }).createTableIfNotExists('presets', function (table) {
    table.bigIncrements('id').primary();
    table.string('presetName');
    addBaseColumns(knex, table);
  });
};

exports.down = function(knex) {
  return knex.schema
    .dropTableIfExists('presets')
    .dropTableIfExists('habits')
    .dropTableIfExists('users');
};

