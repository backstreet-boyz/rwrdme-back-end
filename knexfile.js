// Update with your config settings.
// const app = require('./src/app');
const path = require('path');
// const { client } = app.get('postgres');
require('dotenv').config();

module.exports = {

  development: {
    client: 'pg',
    connection: {
      database: 'rwrdme'
    },
    migrations: {
      directory: path.join(__dirname, 'migrations'),
      tableName: 'migrations'
    },
    seeds: {
      directory: __dirname + '/db/seeds/production'
    }
  },

  test: {
    client: 'pg',
    connection: {
      database: 'rwrdme_test'
    },
    migrations: {
      directory: path.join(__dirname, 'migrations'),
      tableName: 'migrations'
    },
    seeds: {
      directory: __dirname + '/db/seeds/test'
    }
  },

  staging: {
    client: 'pg',
    connection: {
      database: 'rwrdme',
      user: 'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'pg',
    connection: {
      host : process.env.DATABASE_HOST,
      database: process.env.DATABASE_NAME,
      user: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      ssl:true
    },
    pool: {
      min: 2,
      max: 10
    },
    seeds: {
      directory: __dirname + '/db/seeds/production'
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
