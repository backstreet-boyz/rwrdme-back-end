# Rwrdme-back-end

Rwrdme-back-end repository includes Feathers (Node.js) based back-end of web service aimed for individuals to keep up their daily habits.

### WIP Demo:[backstreet-boyz.gitlab.io/rwrdme-front-end](https://backstreet-boyz.gitlab.io/rwrdme-front-end)


#### Front-end repository
- [Rwrdme-front-end](https://gitlab.com/samu.mikkonen/rwrdme-front-end)

## Back-end

- **[Feathers](https://feathersjs.com/)**
- **[Knex.js](http://knexjs.org/)** - Migration management
- **[Objection.js](http://vincit.github.io/objection.js)** - ORM
- **[feathers-objection](https://github.com/mcchrish/feathers-objection)** - Objection.js feathers adapter

### Continuous integration
- Gitlab CI deploying to [Zeit Now](https://zeit.co/docs)
- Database for Demo Hosted on [Heroku PostgreSQL] (https://devcenter.heroku.com/categories/database)

#### Feathers Documentation

This project was bootstrapped with [Feathers](https://feathersjs.com/).

Refer Feathers guide how to perform common tasks.<br>
You can find the most recent version of guide [here](https://docs.feathersjs.com/).

