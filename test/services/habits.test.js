const assert = require('assert');
const app = require('../../src/app');

process.env.NODE_ENV = 'test';

var chai = require('chai');
// var should = chai.should();
var chaiHttp = require('chai-http');

chai.use(chaiHttp);

describe('\'habits\' service', () => {
  it('registered the service', () => {
    const service = app.service('habits');

    assert.ok(service, 'Registered the service');
  });  
});
