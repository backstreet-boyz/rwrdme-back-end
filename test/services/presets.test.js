const assert = require('assert');
const app = require('../../src/app');

describe('\'presets\' service', () => {
  it('registered the service', () => {
    const service = app.service('presets');

    assert.ok(service, 'Registered the service');
  });
});
