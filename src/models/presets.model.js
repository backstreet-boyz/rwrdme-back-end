/* eslint-disable no-console */


const Model = require('objection').Model;

class Presets extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'presets';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['presetName'],

      properties: {
        id: {type: 'integer'},
        presetName: {type: 'string'},
        created_at: {type:'timestamp'},
        updated_at: {type:'timestamp'},
      }
    };
  }

  // This object defines the relations to other models.
/*  static get relationMappings() {
    return {
      owner: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + '/Person',
        join: {
          from: 'Animal.ownerId',
          to: 'Person.id'
        }
      }
    };
  }*/
}

module.exports = Presets;
