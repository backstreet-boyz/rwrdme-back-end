/* eslint-disable no-console */


const Model = require('objection').Model;

class Habits extends Model {
  // Table name is the only required property.
  static get tableName() {
    return 'habits';
  }

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['habitName'],

      properties: {
        id: {type: 'integer'},
        habitName: {type: 'string'},
        timesDoneSuccessively: {type: ['integer', 'null']},
        cue: {type: 'string'},
        duration: {type: 'integer'},
        reward: {type: ['string', 'null']},
        createdAt: {type:'string', format: 'date-time'},
        lastCompleted: {type:['string', 'null'], format: 'date-time'},
        updatedAt: {type:'string', format: 'date-time'},
      }
    };
  }

  // This object defines the relations to other models.
/*  static get relationMappings() {
    return {
      owner: {
        relation: Model.BelongsToOneRelation,
        // The related model. This can be either a Model subclass constructor or an
        // absolute file path to a module that exports one. We use the file path version
        // here to prevent require loops.
        modelClass: __dirname + '/Person',
        join: {
          from: 'Animal.ownerId',
          to: 'Person.id'
        }
      }
    };
  }*/
}

module.exports = Habits;
