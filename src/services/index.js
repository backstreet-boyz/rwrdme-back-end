const users = require('./users/users.service.js');
const habits = require('./habits/habits.service.js');
const presets = require('./presets/presets.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(habits);
  app.configure(presets);
};