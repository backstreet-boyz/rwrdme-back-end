// Initializes the `habits` service on path `/habits`
const habits = require('../../models/habits.model');
const ObjectionService = require('feathers-objection');
const hooks = require('./habits.hooks');
const filters = require('./habits.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: habits,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/habits', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('habits');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
