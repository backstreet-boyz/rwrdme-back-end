const logger = require('winston');


module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [
      function (hook) {
        logger.info('Hook data in after:', hook.data);
      }
    ],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
