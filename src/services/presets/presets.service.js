// Initializes the `presets` service on path `/presets`
const presets = require('../../models/presets.model');
const ObjectionService = require('feathers-objection');
const hooks = require('./presets.hooks');
const filters = require('./presets.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    model: presets,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/presets', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('presets');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
