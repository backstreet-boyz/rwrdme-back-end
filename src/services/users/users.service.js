// Initializes the `users` service on path `/users`
const users = require('../../models/users.model');
const ObjectionService = require('feathers-objection');
const hooks = require('./users.hooks');
const filters = require('./users.filters');

module.exports = function () {
  const app = this;
  // const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    model: users,
    id: 'id',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/users', ObjectionService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('users');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
