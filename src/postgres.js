const knex = require('knex');
const Model = require('objection').Model;
const environment = process.env.NODE_ENV || 'development';
const config = require('../knexfile.js')[environment];
const parse = require('pg-connection-string').parse;


module.exports = function () {
  const app = this;
  const { client, connection } = app.get('postgres');
  const connectionSSL = connection + '?ssl=true';
  let db;

  const configSSL = parse(connectionSSL);

  if(environment === 'production'){
    db = knex({ client, connection: configSSL });
  } else {
    db = knex(config);
  }


  Model.knex(db);

  app.set('knexClient', db);
};
