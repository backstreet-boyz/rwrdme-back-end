/* eslint-disable no-console */
var envPath = __dirname + '/../.env';
require('dotenv').config({path:envPath});
const logger = require('winston');
const app = require('./app');
const port = app.get('port');
const server = app.listen(port);

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

server.on('listening', () => {
  logger.info(`Feathers application started on ${app.get('host')}:${port}`);
  logger.info(`Enviroment is: ${process.env.NODE_ENV}`);
  logger.info(`Database url is: ${process.env.DATABASE_URL}`);
});
